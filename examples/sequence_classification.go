package main

import (
	"flag"
	"fmt"

	"gitlab.com/gamerscomplete/goface/transformers"
)

func main() {
	modelNameFlag := flag.String("model", "distilbert-base-uncased-finetuned-sst-2-english", "Name of the Hugging Face Transformers model")
	textFlag := flag.String("text", "I love using Hugging Face Transformers!", "Text to perform inference on")

	flag.Parse()

	transformers.Initialize(*modelNameFlag, "sequence_classification")
	inference := transformers.Infer(*textFlag)

	fmt.Printf("Inference result for \"%s\":\nLabel: %s\nScore: %f\n", *textFlag, inference.Label, inference.Score)
}
