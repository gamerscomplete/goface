# Transformers Inference in Go

This project demonstrates how to use the Hugging Face Transformers library in a Go program by leveraging Python and C.

## Requirements

- Go
- Python 3.10
- Hugging Face Transformers Python library
- libpython3.10-dev (or the corresponding version of libpython for your Python version)

## Structure

- `transformers`: Main package containing the Go code for the Transformers Inference wrapper.
    - `transformers.go`: Main file for the package.
    - `inference.go`: Functions for initializing the Transformers Inference wrapper and performing inference.
    - `cgo`: Directory containing the C code for interfacing with Python.
        - `transformers_inference_wrapper.h`: Header file containing the declarations for the C functions.
        - `transformers_inference_wrapper.c`: C implementation of the functions to interface with Python.
        - `transformers_inference.py`: Python file containing the Hugging Face Transformers code for model loading and inference.
- `examples`: Directory containing example code for using the Transformers Inference wrapper in Go.

## Usage

1. Install the required Python libraries:

```sh
pip install transformers torch
```

2. Build the Go package:

```sh
go build ./transformers
```

3. Run the example code:

```sh
go run ./examples
```

This project allows you to use different model loaders from the Hugging Face Transformers library, including:

- `AutoModel.from_pretrained`
- `AutoModelForSeq2LM.from_pretrained`
- `AutoModelForCausalLM.from_pretrained`

To use a different loader, pass the appropriate loader type string as a parameter when initializing the Transformers Inference wrapper:

```go
// Initialize the Transformers Inference wrapper with the desired model and loader type
transformers.Initialize("distilbert-base-uncased", "AutoModelForSequenceClassification", "./transformers/cgo")
```


Known Limitations
- The current implementation supports only a limited set of model types.
- The project is not optimized for performance and may not be suitable for high-throughput or low-latency applications.
- The integration with Python and C adds complexity and may require additional debugging and troubleshooting.


## Customizing the Transformers Inference Wrapper

You can easily extend the Transformers Inference wrapper to support additional functionality from the Hugging Face Transformers library. To do so, modify the Python and C code accordingly, and expose new functions through the Go code.

### Adding new model loaders

1. Update `transformers_inference.py` to import the required model loader and create a new `initialize_*` function for it.
2. Update `transformers_inference_wrapper.c` and `transformers_inference_wrapper.h` to expose the new `initialize_*` function to the C code.
3. Update `inference.go` to call the new `initialize_*` function based on the loader type provided when initializing the Transformers Inference wrapper.

### Extending inference functionality

1. Update `transformers_inference.py` to implement any additional functionality you need.
2. Expose the new functionality in `transformers_inference_wrapper.c` and `transformers_inference_wrapper.h`.
3. Create new Go functions in `inference.go` that call the corresponding C functions to utilize the new functionality.

## Troubleshooting

If you encounter issues while using the Transformers Inference wrapper, try the following:

1. Ensure that your Python environment and library requirements are installed correctly.
2. Verify that the paths in `inference.go` and `transformers_inference_wrapper.c` are correctly set to locate the `transformers/cgo` directory and the `transformers_inference.py` file.
3. Check the C and Go code for any inconsistencies in function declarations or missing imports.
4. Consult the Hugging Face Transformers documentation for guidance on using the library and its various components.

## Contributing

We welcome contributions to improve and extend the Transformers Inference wrapper. If you have ideas or suggestions, feel free to open an issue or submit a pull request.

## License

This project is licensed under the [MIT License](LICENSE).

