package transformers

// #cgo CFLAGS: -I .
// #cgo CFLAGS: -I ./cgo
// #cgo CFLAGS: -I /usr/include/python3.10
// #cgo LDFLAGS: -L /usr/lib/x86_64-linux-gnu -lpython3.10
// #include "cgo/transformers_inference_wrapper.c"
import "C"

import (
	"path/filepath"
	"runtime"
	"unsafe"
)

type InferenceResult struct {
	Label string
	Score float64
}

func Initialize(modelName, loaderType string) {
	_, currentFilePath, _, _ := runtime.Caller(0)
	currentDir := filepath.Dir(currentFilePath)
	cgoPath := filepath.Join(currentDir, "cgo")

	cModelName := C.CString(modelName)
	cCgoPath := C.CString(cgoPath)
	cLoaderType := C.CString(loaderType)
	defer C.free(unsafe.Pointer(cModelName))
	defer C.free(unsafe.Pointer(cCgoPath))
	defer C.free(unsafe.Pointer(cLoaderType))
	C.initialize(cModelName, cLoaderType, cCgoPath)
}

func Infer(text string) InferenceResult {
	cText := C.CString(text)
	defer C.free(unsafe.Pointer(cText))
	inference := C.infer(cText)
	return InferenceResult{
		Label: C.GoString(inference.label),
		Score: float64(inference.score),
	}
}
