#include "transformers_inference_wrapper.h"
#include <stdlib.h>
#include <Python.h>


static PyObject *instance = NULL;

void initialize(const char *model_name, const char *loader_type, const char *cgo_path) {

    Py_Initialize();

    PyObject *sysPath = PySys_GetObject("path");

    PyObject *path = PyUnicode_FromString(cgo_path);
    PyList_Append(sysPath, path);
    Py_DECREF(path);

    PyObject *module_name = PyUnicode_FromString("transformers_inference");
    PyObject *module = PyImport_Import(module_name);
    Py_DECREF(module_name);

    if (module == NULL) {
        PyErr_Print();
        exit(1);
    }

    //Set the model loader type
    PyObject *loader_type_py = PyUnicode_FromString(loader_type);
    PyObject *args = PyTuple_New(2);

    PyTuple_SetItem(args, 1, loader_type_py);


    // setup the initialize function to call
    PyObject *class = PyObject_GetAttrString(module, "initialize");
    Py_DECREF(module);

    if (class == NULL) {
        PyErr_Print();
        exit(1);
    }

    //Execute the initialize function
    PyObject *model_name_py = PyUnicode_FromString(model_name);

    PyTuple_SetItem(args, 0, model_name_py);
    instance = PyObject_CallObject(class, args);
    Py_DECREF(args);

    if (instance == NULL) {
        PyErr_Print();
        exit(1);
    }
}

InferenceResult infer(const char *text) {
    PyObject *py_module, *py_func, *py_args, *py_result;
    InferenceResult result;

    py_module = PyImport_ImportModule("transformers_inference");
    if (!py_module) {
        PyErr_Print();
        result.label = "Error importing module";
        result.score = -1;
        return result;
    }

    py_func = PyObject_GetAttrString(py_module, "infer");
    if (!py_func || !PyCallable_Check(py_func)) {
        PyErr_Print();
        result.label = "Error getting function";
        result.score = -1;
        return result;
    }

    py_args = PyTuple_New(1);
    PyTuple_SetItem(py_args, 0, PyUnicode_FromString(text));

    py_result = PyObject_CallObject(py_func, py_args);
    if (!py_result) {
        PyErr_Print();
        result.label = "Error calling function";
        result.score = -1;
        return result;
    }

    result.label = PyUnicode_AsUTF8(PyTuple_GetItem(py_result, 0));
    result.score = PyFloat_AsDouble(PyTuple_GetItem(py_result, 1));

    Py_XDECREF(py_result);
    Py_XDECREF(py_args);
    Py_XDECREF(py_func);
    Py_XDECREF(py_module);

    return result;
}
