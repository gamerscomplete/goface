from transformers import (
    AutoTokenizer,
    AutoModel,
    AutoModelForSequenceClassification,
    AutoModelForSeq2SeqLM,
    AutoModelForCausalLM,
)

import torch

_model = None
_tokenizer = None

def initialize(model_name, loader_type):
    global _model, _tokenizer
    _tokenizer = AutoTokenizer.from_pretrained(model_name, use_fast=True)

    kwargs = {"torch_dtype": torch.float16}
    kwargs["device_map"] = "auto"

    if loader_type == "sequence_classification":
        _model = AutoModelForSequenceClassification.from_pretrained(model_name)
    elif loader_type == "seq2seq":
        _model = AutoModelForSeq2SeqLM.from_pretrained(model_name, **kwargs)
    elif loader_type == "causal":
        _model = AutoModelForCausalLM.from_pretrained(model_name, **kwargs)
    else:
        _model = AutoModel.from_pretrained(model_name, **kwargs)

    model.to(device)


def infer(text):
    global _model, _tokenizer
    if _model is None or _tokenizer is None:
        raise ValueError("Model not initialized")

    inputs = _tokenizer(text, return_tensors="pt")
    outputs = _model(**inputs)
    probabilities = torch.softmax(outputs.logits, dim=-1)
    max_prob, max_index = torch.max(probabilities, dim=-1)

    return _model.config.id2label[max_index.item()], max_prob.item()

