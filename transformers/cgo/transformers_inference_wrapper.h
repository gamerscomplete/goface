#ifndef TRANSFORMERS_INFERENCE_WRAPPER_H
#define TRANSFORMERS_INFERENCE_WRAPPER_H

typedef struct {
    const char *label;
    double score;
} InferenceResult;

void initialize(const char *model_name, const char *loader_type, const char *cgo_path);
InferenceResult infer(const char *text);

#endif // TRANSFORMERS_INFERENCE_WRAPPER_H
