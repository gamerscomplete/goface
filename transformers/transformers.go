package transformers

// #cgo CFLAGS: -I${SRCDIR}
// #cgo CFLAGS: -I${SRCDIR}/cgo
// #cgo CFLAGS: -I /usr/include/python3.10
// #cgo LDFLAGS: -L /usr/lib/x86_64-linux-gnu -lpython3.10
// #include "cgo/transformers_inference_wrapper.h"
